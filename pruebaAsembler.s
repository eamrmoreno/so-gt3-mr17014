.data
sp: .asciz "Introduce 2 num: (Ej: num1 num2)\n"
ss: .asciz "%d %d"
sp2: .asciz "Los numeros son : %d y %d. \nEl resultado de la suma es: %d. \n"

.bss
.comm n1 , 4 , 4
.comm n2 , 4 , 4
.comm total , 4 , 4

.text
.global main

main :
#imprime el mensaje alojado en la constante sp definida en .data
pushl $sp
call printf
//eliminar
addl $4 , %esp

#Guarda los números en estructura FILO/LIFO
pushl $n2
pushl $n1
#imprime el mensaje alojado en la constante ss definida en .data
pushl $ss
call scanf

#suma
movl n1 , %eax
addl n2 , %eax
movl %eax , total

pushl total
pushl n2
pushl n1
pushl $sp2
call printf

movl $0 , %ebx
movl $1 , %eax
int $0x80
